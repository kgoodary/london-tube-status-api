var restify = require('restify');
var request = require('request');
var _		= require('lodash-node');

var server = restify.createServer({
  name: 'tfl-tube-status-api',
});

server.use(restify.CORS());
server.use(restify.fullResponse());

var tflStatusLink = 'https://api.tfl.gov.uk/line/mode/tube/status';

function transformFeed(statusJson, callback) {

	var tranformedJson = [];

	_.each(statusJson, function(v, k) {

		tranformedJson.push({
			id: v.id,
			name: v.name,
			created: v.created,
			modified: v.modified,
			lineStatuses: v.lineStatuses,
			routeSections: v.routeSections
		});

	});

	callback(tranformedJson);

}

function returnAll(req, res, next) {
	
	request({
		url: tflStatusLink,
		json: true
	}, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			
			transformFeed(body, function(transformedJson) {

				res.send(transformedJson);

			});

		}
	});

	return next();
}

server.get('/api/status/all', returnAll);
server.listen(process.env.PORT || 9988);
